package ir.divar.interview.library.base.data.model

data class ErrorResponse(
    val code: Int,
    val description: String,
    val error: String,
    val status: Int,
    val userMessage: String
)
