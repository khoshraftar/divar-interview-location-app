package ir.divar.interview.library.base.domain

import ir.divar.interview.library.base.data.model.ErrorResponse

sealed class ResultWrapper<out T> {
    data class Success<out T>(val code: Int? = null, val body: T) : ResultWrapper<T>()
    data class GenericError(val code: Int? = null, val error: ErrorResponse? = null) :
        ResultWrapper<Nothing>()

    object NetworkError : ResultWrapper<Nothing>()
}
