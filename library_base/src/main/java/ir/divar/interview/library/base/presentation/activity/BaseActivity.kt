package ir.divar.interview.library.base.presentation.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import timber.log.Timber

abstract class BaseActivity(@LayoutRes contentLayoutId: Int) : InjectionActivity(contentLayoutId) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide()

        Timber.v("onCreate ${javaClass.simpleName}")
    }
}
