package ir.divar.interview.library.base.domain

import com.google.gson.Gson
import ir.divar.interview.library.base.data.model.ErrorResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

object SafeApi {
    suspend fun <T> safeApiCall(
        dispatcher: CoroutineDispatcher,
        apiCall: suspend () -> T
    ): ResultWrapper<T> {
        return withContext(dispatcher) {
            @Suppress("UNCHECKED_CAST")
            try {
                val response: Response<T> = apiCall.invoke() as Response<T>
                if (response.isSuccessful) {
                    ResultWrapper.Success(code = response.code(), body = response)
                } else {
                    handleNetworkError(throwable = Throwable(response.errorBody().toString()))
                }
            } catch (throwable: Throwable) {
                handleNetworkError(throwable)
            } as ResultWrapper<T>
        }
    }

    private fun handleNetworkError(throwable: Throwable): ResultWrapper<Nothing> {
        return when (throwable) {
            is IOException -> ResultWrapper.NetworkError
            is HttpException -> {
                val code = throwable.code()
                val errorResponse =
                    convertErrorBody(
                        throwable
                    )
                ResultWrapper.GenericError(code, errorResponse)
            }
            else -> {
                ResultWrapper.GenericError(null, null)
            }
        }
    }

    private fun convertErrorBody(throwable: HttpException): ErrorResponse? {
        return try {
            throwable.response()?.errorBody()?.string()?.let {
                Gson().fromJson(it, ErrorResponse::class.java)
            }
        } catch (exception: Exception) {
            null
        }
    }
}
