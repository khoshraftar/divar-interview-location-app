package ir.divar.interview.library.base.util

import kotlin.math.floor

@Suppress("IMPLICIT_CAST_TO_ANY")
object TimeUtils {
    fun formatSeconds(timeInSeconds: Int): String {
        val secondsLeft = timeInSeconds % 3600 % 60
        val minutes = floor((timeInSeconds % 3600 / 60).toDouble()).toInt()
//        val hours = floor((timeInSeconds / 3600).toDouble()).toInt()

        val formattedMinutes = if (minutes < 10) "0$minutes" else minutes
        val formattedSeconds = if (secondsLeft < 10) "0$secondsLeft" else secondsLeft

        return "$formattedMinutes:$formattedSeconds"
    }

    fun formatSecondsTrimed(timeInSeconds: Int): String {
        val secondsLeft = timeInSeconds % 3600 % 60
        val minutes = floor((timeInSeconds % 3600 / 60).toDouble()).toInt()

        val formattedMinutes = if (minutes < 10) "$minutes" else minutes
        val formattedSeconds = if (secondsLeft < 10) "0$secondsLeft" else secondsLeft

        return "$formattedMinutes:$formattedSeconds"
    }
}
