package ir.divar.interview.app.data.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import ir.divar.interview.app.domain.model.ExploreDomainModel

@Keep
data class ExploreDataModel(
    @SerializedName("meta")
    val meta: Meta,
    @SerializedName("response")
    val response: Response
) {
    @Keep
    data class Meta(
        @SerializedName("code")
        val code: Int,
        @SerializedName("requestId")
        val requestId: String
    )

    @Keep
    data class Response(
        @SerializedName("suggestedFilters")
        val suggestedFilters: SuggestedFilters,
        @SerializedName("warning")
        val warning: Warning,
        @SerializedName("suggestedRadius")
        val suggestedRadius: Int,
        @SerializedName("headerLocation")
        val headerLocation: String,
        @SerializedName("headerFullLocation")
        val headerFullLocation: String,
        @SerializedName("headerLocationGranularity")
        val headerLocationGranularity: String,
        @SerializedName("totalResults")
        val totalResults: Int,
        @SerializedName("suggestedBounds")
        val suggestedBounds: SuggestedBounds,
        @SerializedName("groups")
        val groups: List<Group>
    ) {
        @Keep
        data class SuggestedFilters(
            @SerializedName("header")
            val header: String,
            @SerializedName("filters")
            val filters: List<Filter>
        ) {
            @Keep
            data class Filter(
                @SerializedName("name")
                val name: String,
                @SerializedName("key")
                val key: String
            )
        }

        @Keep
        data class Warning(
            @SerializedName("text")
            val text: String
        )

        @Keep
        data class SuggestedBounds(
            @SerializedName("ne")
            val ne: Ne,
            @SerializedName("sw")
            val sw: Sw
        ) {
            @Keep
            data class Ne(
                @SerializedName("lat")
                val lat: Double,
                @SerializedName("lng")
                val lng: Double
            )

            @Keep
            data class Sw(
                @SerializedName("lat")
                val lat: Double,
                @SerializedName("lng")
                val lng: Double
            )
        }

        @Keep
        data class Group(
            @SerializedName("type")
            val type: String,
            @SerializedName("name")
            val name: String,
            @SerializedName("items")
            val items: List<Item>
        ) {
            @Keep
            data class Item(
                @SerializedName("reasons")
                val reasons: Reasons,
                @SerializedName("venue")
                val venue: Venue,
                @SerializedName("referralId")
                val referralId: String
            ) {
                @Keep
                data class Reasons(
                    @SerializedName("count")
                    val count: Int,
                    @SerializedName("items")
                    val items: List<Item>
                ) {
                    @Keep
                    data class Item(
                        @SerializedName("summary")
                        val summary: String,
                        @SerializedName("type")
                        val type: String,
                        @SerializedName("reasonName")
                        val reasonName: String
                    )
                }

                @Keep
                data class Venue(
                    @SerializedName("id")
                    val id: String,
                    @SerializedName("name")
                    val name: String,
                    @SerializedName("location")
                    val location: Location,
                    @SerializedName("categories")
                    val categories: List<Category>,
                    @SerializedName("photos")
                    val photos: Photos
                ) {
                    @Keep
                    data class Location(
                        @SerializedName("address")
                        val address: String,
                        @SerializedName("lat")
                        val lat: Double,
                        @SerializedName("lng")
                        val lng: Double,
                        @SerializedName("labeledLatLngs")
                        val labeledLatLngs: List<LabeledLatLng>,
                        @SerializedName("distance")
                        val distance: Int,
                        @SerializedName("postalCode")
                        val postalCode: String,
                        @SerializedName("cc")
                        val cc: String,
                        @SerializedName("neighborhood")
                        val neighborhood: String,
                        @SerializedName("city")
                        val city: String,
                        @SerializedName("state")
                        val state: String,
                        @SerializedName("country")
                        val country: String,
                        @SerializedName("formattedAddress")
                        val formattedAddress: List<String>
                    ) {
                        @Keep
                        data class LabeledLatLng(
                            @SerializedName("label")
                            val label: String,
                            @SerializedName("lat")
                            val lat: Double,
                            @SerializedName("lng")
                            val lng: Double
                        )
                    }

                    @Keep
                    data class Category(
                        @SerializedName("id")
                        val id: String,
                        @SerializedName("name")
                        val name: String,
                        @SerializedName("pluralName")
                        val pluralName: String,
                        @SerializedName("shortName")
                        val shortName: String,
                        @SerializedName("icon")
                        val icon: Icon,
                        @SerializedName("primary")
                        val primary: Boolean
                    ) {
                        @Keep
                        data class Icon(
                            @SerializedName("prefix")
                            val prefix: String,
                            @SerializedName("suffix")
                            val suffix: String
                        )
                    }

                    @Keep
                    data class Photos(
                        @SerializedName("count")
                        val count: Int,
                        @SerializedName("groups")
                        val groups: List<Any>
                    )
                }
            }
        }
    }
}

internal fun ExploreDataModel.toDomainModel() = ExploreDomainModel(
    venues = this.response.groups[0].items.map {
        it.venue
    }
)
