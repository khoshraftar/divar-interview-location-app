package ir.divar.interview.app.presentation

import androidx.fragment.app.Fragment
import coil.ImageLoader
import ir.divar.interview.app.presentation.list.ListViewModel
import ir.divar.interview.app.presentation.list.VenueAdapter
import ir.divar.interview.library.base.di.KotlinViewModelProvider
import org.kodein.di.Kodein
import org.kodein.di.android.x.AndroidLifecycleScope
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.scoped
import org.kodein.di.generic.singleton

val presentationModule = Kodein.Module("PresentationModule") {

    bind<ListViewModel>() with scoped<Fragment>(AndroidLifecycleScope).singleton {
        KotlinViewModelProvider.of(context) {
            ListViewModel(
                instance(),
                instance()
            )
        }
    }

    bind() from singleton { VenueAdapter() }

    bind() from singleton { ImageLoader(instance()) }
}
