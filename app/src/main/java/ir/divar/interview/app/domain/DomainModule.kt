package ir.divar.interview.app.domain

import ir.divar.interview.app.domain.usecase.AppUseCase
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val domainModule = Kodein.Module("DomainModule") {
    bind() from singleton { AppUseCase(instance()) }
}
