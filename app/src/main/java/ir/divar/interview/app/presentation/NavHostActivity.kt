package ir.divar.interview.app.presentation

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import ir.divar.interview.R
import ir.divar.interview.library.base.presentation.activity.BaseActivity
import ir.divar.interview.library.base.presentation.navigation.NavManager
import kotlinx.android.synthetic.main.activity_nav_host.*
import org.kodein.di.generic.instance

class NavHostActivity : BaseActivity(R.layout.activity_nav_host) {

    private val navController get() = navHostFragment.findNavController()

    private val navManager: NavManager by instance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initNavManager()
    }

    private fun initNavManager() {
        navManager.setOnNavEvent {
            navController.navigate(it)
        }
    }
}
