package ir.divar.interview.app.presentation.list

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import ir.divar.interview.R
import ir.divar.interview.library.base.presentation.extension.observe
import ir.divar.interview.library.base.presentation.fragment.InjectionFragment
import kotlinx.android.synthetic.main.fragment_list.*
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class ListFragment : InjectionFragment(R.layout.fragment_list), KodeinAware {

    private val viewModel: ListViewModel by instance()

    private val venueAdapter: VenueAdapter by instance()

    private val stateObserver = Observer<ListViewModel.ViewState> {
//        progressBar.visible = it.isLoading
        if (it.exploreModel != null) {
            venueAdapter.venues = it.exploreModel.venues
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        venueAdapter.setOnDebouncedClickListener {

        }

        listRV.apply {
            setHasFixedSize(true)
            adapter = venueAdapter
        }

        observe(viewModel.stateLiveData, stateObserver)
        viewModel.loadData()
    }
}
