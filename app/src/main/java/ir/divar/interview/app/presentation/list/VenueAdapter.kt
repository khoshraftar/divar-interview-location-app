package ir.divar.interview.app.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.divar.interview.R
import ir.divar.interview.app.data.model.ExploreDataModel
import ir.divar.interview.library.base.delegate.observer
import ir.divar.interview.library.base.presentation.extension.setOnDebouncedClickListener

internal class VenueAdapter : RecyclerView.Adapter<VenueAdapter.CategoryViewHolder>() {

    var venues: List<ExploreDataModel.Response.Group.Item.Venue> by observer(listOf()) {
        notifyDataSetChanged()
    }

    private var onDebouncedClickListener: ((venues: ExploreDataModel.Response.Group.Item.Venue) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return CategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bind(venues[position])
    }

    override fun getItemCount(): Int = venues.size

    fun setOnDebouncedClickListener(listener: (venue: ExploreDataModel.Response.Group.Item.Venue) -> Unit) {
        this.onDebouncedClickListener = listener
    }

    internal inner class CategoryViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(venue: ExploreDataModel.Response.Group.Item.Venue) {
            itemView.setOnDebouncedClickListener { onDebouncedClickListener?.invoke(venue) }
        }
    }
}
