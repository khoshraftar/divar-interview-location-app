package ir.divar.interview.app.data

import ir.divar.interview.app.data.repository.RepositoryImpl
import ir.divar.interview.app.data.retrofit.service.RetrofitService
import ir.divar.interview.app.domain.repository.AppRepository
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit

val dataModule = Kodein.Module("DataModule") {

    bind<AppRepository>() with singleton { RepositoryImpl(instance()) }

    bind() from singleton { instance<Retrofit>().create(RetrofitService::class.java) }
}
