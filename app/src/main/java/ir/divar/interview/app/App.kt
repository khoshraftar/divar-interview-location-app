package ir.divar.interview.app

import androidx.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import ir.divar.interview.BuildConfig
import ir.divar.interview.app.data.dataModule
import ir.divar.interview.app.di.appModule
import ir.divar.interview.app.di.kodein.FragmentArgsExternalSource
import ir.divar.interview.app.domain.domainModule
import ir.divar.interview.app.presentation.presentationModule
import ir.divar.interview.library.base.baseModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import timber.log.Timber

class App : MultiDexApplication(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@App))
        import(baseModule)
        import(appModule)

        import(presentationModule)
        import(domainModule)
        import(dataModule)

        externalSources.add(FragmentArgsExternalSource())
    }

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
