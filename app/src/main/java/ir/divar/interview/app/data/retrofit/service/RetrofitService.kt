package ir.divar.interview.app.data.retrofit.service

import ir.divar.interview.app.data.model.ExploreDataModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

internal interface RetrofitService {
    @GET("explore")
    suspend fun nearVenues(@Query("ll") latLng: String, @Query("limit") limit: Int = 10): Response<ExploreDataModel>
}
