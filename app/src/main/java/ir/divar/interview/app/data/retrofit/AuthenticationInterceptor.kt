package ir.divar.interview.app.data.retrofit

import okhttp3.Interceptor
import okhttp3.Response

class AuthenticationInterceptor(private val clientId: String, private val clientSecret: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response = chain.request().let {
        val url = it.url.newBuilder()
            .addQueryParameter("client_id", clientId)
            .addQueryParameter("client_secret", clientSecret)
            .build()

        val newRequest = it.newBuilder()
            .url(url)
            .build()

        chain.proceed(newRequest)
    }
}
