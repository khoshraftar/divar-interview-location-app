package ir.divar.interview.app.domain.usecase

import ir.divar.interview.app.data.model.ExploreDataModel
import ir.divar.interview.app.domain.repository.AppRepository
import ir.divar.interview.library.base.domain.ResultWrapper
import retrofit2.Response

internal class AppUseCase(
    private val appRepository: AppRepository
) {

    suspend fun getNearVenues(latLng: String, limit: Int): ResultWrapper<Response<ExploreDataModel>> {
        return appRepository.getNearVenues(latLng, limit)
    }
}
