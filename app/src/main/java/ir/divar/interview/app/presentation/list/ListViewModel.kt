package ir.divar.interview.app.presentation.list

import androidx.lifecycle.viewModelScope
import ir.divar.interview.app.data.model.ExploreDataModel
import ir.divar.interview.app.data.model.toDomainModel
import ir.divar.interview.app.domain.model.ExploreDomainModel
import ir.divar.interview.app.domain.usecase.AppUseCase
import ir.divar.interview.library.base.domain.ResultWrapper
import ir.divar.interview.library.base.presentation.navigation.NavManager
import ir.divar.interview.library.base.presentation.viewmodel.BaseAction
import ir.divar.interview.library.base.presentation.viewmodel.BaseViewModel
import ir.divar.interview.library.base.presentation.viewmodel.BaseViewState
import kotlinx.coroutines.*

internal class ListViewModel(
    private val navManager: NavManager,
    private val appUseCase: AppUseCase
) : BaseViewModel<ListViewModel.ViewState, ListViewModel.Action>(
    ViewState()
) {

    override fun onLoadData() {
    }

    override fun onReduceState(viewAction: Action) = when (viewAction) {
        is Action.Success -> state.copy(
            isLoading = false,
            isError = false,
            exploreModel = viewAction.exploreData.toDomainModel()
        )
        is Action.Failure -> state.copy(
            isLoading = false,
            isError = true,
            exploreModel = null
        )
    }

    private fun getNearVenues(latLng: String, limit: Int = 10) {
        viewModelScope.launch {
            appUseCase.getNearVenues(latLng, limit).also { result ->
                val action = when (result) {
                    is ResultWrapper.Success -> Action.Success(
                        result.body.body()!!
                    )
                    is ResultWrapper.GenericError -> Action.Failure
                    is ResultWrapper.NetworkError -> Action.Failure
                }
                sendAction(action)
            }
        }
    }

    internal data class ViewState(
        val isLoading: Boolean = true,
        val isError: Boolean = false,
        val exploreModel: ExploreDomainModel? = null
    ) : BaseViewState

    internal sealed class Action :
        BaseAction {
        class Success(val exploreData: ExploreDataModel) : Action()
        object Failure : Action()
    }
}
