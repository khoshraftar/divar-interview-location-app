package ir.divar.interview.app.domain.model

import ir.divar.interview.app.data.model.ExploreDataModel

data class ExploreDomainModel(
    val venues: List<ExploreDataModel.Response.Group.Item.Venue>
)
