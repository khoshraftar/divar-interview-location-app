package ir.divar.interview.app.domain.repository

import ir.divar.interview.app.data.model.ExploreDataModel
import ir.divar.interview.library.base.domain.ResultWrapper
import retrofit2.Response

internal interface AppRepository {
    suspend fun getNearVenues(latLng: String, limit: Int = 10): ResultWrapper<Response<ExploreDataModel>>
}
