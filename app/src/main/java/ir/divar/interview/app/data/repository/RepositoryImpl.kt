package ir.divar.interview.app.data.repository

import ir.divar.interview.app.data.model.ExploreDataModel
import ir.divar.interview.app.data.retrofit.service.RetrofitService
import ir.divar.interview.app.domain.repository.AppRepository
import ir.divar.interview.library.base.domain.ResultWrapper
import ir.divar.interview.library.base.domain.SafeApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Response

internal class RepositoryImpl(
    private val retrofitService: RetrofitService,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : AppRepository {

    override suspend fun getNearVenues(latLng: String, limit: Int): ResultWrapper<Response<ExploreDataModel>> {
        return SafeApi.safeApiCall(dispatcher) {
            retrofitService.nearVenues(latLng, limit)
        }
    }
}
